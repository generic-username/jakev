


var gameObjects = [];
function rand(min, max){
    return min + Math.floor(Math.random() * (max-min));
}
function randVelocity2(){
    return (Math.random()*2)-1;
}
function randVelocity(){
    if(Math.random() <= 0.5){
        return -1;
    }else{
        return 1;
    }
}
function random_rgba(a) {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + a + ')';
}
function randomGameObj(){
    if(Math.random() < 0.66){
        gameObjects.push(new component(5, 5, "#0B28E1",//
            rand(0,window.innerWidth - 20),
            rand(0,window.innerHeight - 20),
            randVelocity(),
            randVelocity(),
            0));
    }else{
        gameObjects.push(new component(5, 5, "#c15",//
            rand(0,window.innerWidth - 20),
            rand(0,window.innerHeight - 20),
            randVelocity2(),
            randVelocity2(),
            1));
    }
}
function startGame() {
    let slider = document.getElementById("opacitySlider");
    slider.oninput = function() {
        document.querySelectorAll('.card').forEach(e => e.style.background = 'rgb(100, 100, 100, '+this.value/1000.0+')');
    }
    let cSlider = document.getElementById("countSlider");
    cSlider.oninput = function() {

        if(gameObjects.length <= this.value){
            for (let j = 0; j < this.value - gameObjects.length; j++) {
                randomGameObj();
            }
        }
        if(gameObjects.length >= this.value){
            for (let j = 0; j < gameObjects.length - this.value; j++) {
                gameObjects.pop();
            }
        }

    }
    for (i = 0; i < 150; i++) {
        randomGameObj();
    }

    myGameArea.canvas = document.getElementById("myCanvas");
    //2A2A2A
    myGameArea.start();

    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
        console.debug('e1');
        coll[i].addEventListener("click", function() {
            console.debug('clicked');
            this.classList.toggle("active");
            var content = this.nextElementSibling;
            if (content.style.display === "block") {
                content.style.display = "none";
            } else {
                content.style.display = "block";
            }
        });
    }
}

class Rect {
    constructor(x1, x2, y1, y2) {
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
    }
}

var myGameArea = {
    canvas : null,
    start : function() {
        update_canvas();
        this.context = this.canvas.getContext("2d");
        this.interval = setInterval(updateGameArea, 16);
    },
    clear : function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}
var MIN_LOC = 0;
function component(width, height, color, x, y, vx, vy, type) {
    this.type = type;
    this.width = width;
    this.height = height;
    this.x = x;
    this.y = y;
    this.vx = vx;
    this.vy = vy;
    let cols = document.getElementsByClassName("collidable");
    this.isCollidingWithObj = function (x, y){
        return this.getCollidingObject(x, y) !== null;
    }
    this.getCollidingObject = function (x, y){
        for (let i = 0; i < cols.length; i++) {
            let rect = cols[i].getBoundingClientRect();

            let rLeft = rect.left;
            let rRight = rect.right;
            let rTop = rect.top;
            let rBottom = rect.bottom;
            // console.log(rLeft, rRight, rTop, rBottom, "vs", this.x, this.y);

            if(x >= rLeft && x <= rRight){
                if(y >= rTop && y <= rBottom){
                    return new Rect(rLeft, rRight, rTop, rBottom);
                }
            }
        }
        return null;
    }
    this.update = function(){
        // Edge of screen collision X
        if(this.x < MIN_LOC || this.x > myGameArea.canvas.width-MIN_LOC){
            this.vx *= -1;
        }
        if(this.type === 1){
            if(this.isCollidingWithObj(this.x + this.vx, this.y)) {
                this.vx *= -1;
            }
        }
        this.x += this.vx;

        // Edge Y
        if(this.y < MIN_LOC || this.y > myGameArea.canvas.height-MIN_LOC){
            this.vy *= -1;
        }
        if(this.type === 1) {
            if(this.isCollidingWithObj(this.x, this.y + this.vy)) {
                this.vy *= -1;
            }
        }

        this.y += this.vy;

        // Logic for pushing out
        if(this.type === 1){
            let co = this.getCollidingObject(this.x, this.y);
            if(co !== null){
                let midY = (co.y1 + co.y2) / 2.0;
                this.y = (this.y > midY) ? co.y2 : co.y1;
            }
        }



        ctx = myGameArea.context;
        ctx.fillStyle = color;
        ctx.fillRect(this.x, this.y, this.width, this.height);
    }
}

function update_canvas(){
    myGameArea.canvas.width  = window.innerWidth;
    myGameArea.canvas.height = window.innerHeight;
}

function distanceSquared(fromX, fromY, toX, toY){
    var dx = toX-fromX;
    var dy = toY-fromY;
    return (dx*dx) + (dy*dy);
}

function updateGameArea() {
    update_canvas();
    myGameArea.clear();
    var i;
    for (i = 0; i < gameObjects.length; i++) {
        gameObjects[i].update();
    }
    ctx = myGameArea.context;
    ctx.beginPath();
    ctx.rect(0, 0, window.innerWidth, window.innerHeight);
    ctx.fillStyle = "rgba(0, 0, 0, 0.01)";
    ctx.fill();
    var ran = 0;
    for (i = 0; i < gameObjects.length; i++) {
        var objA = gameObjects[i];
        for (j = 0; j < gameObjects.length; j++) {
            var objB = gameObjects[j];
            if(objA === objB){
                continue;
            }

            var distPct = 1.0 - (Math.sqrt(distanceSquared(objA.x, objA.y, objB.x, objB.y)) / 150.0);
            if(distPct > 0){
                ctx.beginPath();
                if(objA.type === 0 && objB.type === 0){
                    ctx.strokeStyle = "rgba(11, 34, 254, "+distPct+")";
                }else if(objA.type === 1 && objB.type === 1){
                    ctx.strokeStyle = "rgba(204, 11, 55, "+distPct+")";
                }else{
                    ctx.strokeStyle = "rgba(107, 22, 154, "+distPct+")";
                }
//                ctx.strokeStyle = objA.color;
                ctx.moveTo(objA.x+3, objA.y+3);
                ctx.lineTo(objB.x+3, objB.y+3);
                ctx.stroke();
                ctx.closePath();
            }
        }
    }
}